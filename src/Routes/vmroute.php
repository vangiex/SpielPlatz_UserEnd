<?php

/*
 * Copyright (C) 2018 VanGiex
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use Slim\Http\Request;
use Slim\Http\Response;

$app->get('/vmadd/{userid}/{name}/{os}/{ip}/{port}/{protocol}[/]', function ($request, $response, $args) {
    $route = $request->getAttribute('route');
    $userid = $route->getArgument('userid');
    $name = $route->getArgument('name');
    $os = $route->getArgument('os');
    $ip = $route->getArgument('ip');
    $port = $route->getArgument('port');
    $protocol = $route->getArgument('protocol');
    $vid = "";
    $username = "";
    $password = "";
    $network = "Isolated";

    // DBCONFIG
    require '../src/Connections.php';

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }


    $sql = "INSERT INTO `VMCONFIGPOOL`(`UID`, `VM_NAME`, `IP`, `PORT`, `PROTOCAL`) VALUES ('$userid','$name','$ip','$port','$protocol')";
    $result = $conn->query($sql);

    switch ($os) {
        case "ubuntu":
            $username = "ubuntu";
            $password = "ubuntu";
            break;
        case "backbox":
            $username = "backbox";
            $password = "backbox";
            break;
        case "xp":
            $username = "xp";
            $password = "xp";
            break;
    }

    $sql = "INSERT INTO `VMDETAILSPOOL`(`VID`, `OS`, `NETWORK_SUPPORT`, `USERNAME`, `PASSWORD`) VALUES ( (SELECT VID FROM VMCONFIGPOOL WHERE VM_NAME = '$name' AND PORT = '$port') , "
            . "'$os','$network','$username','$password')";

    $result = $conn->query($sql);

    $data = array('status' => 'Sucess', 'error' => $result);
    return $response->withJson($data);
});

$app->get('/vmconfig/{userid}/{name}[/]', function ($request, $response, $args) {

    $route = $request->getAttribute('route');

    $userid = $route->getArgument('userid');
    $name = $route->getArgument('name');

    // DBCONFIG
    require '../src/Connections.php';

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "SELECT * FROM VMCONFIGPOOL WHERE UID = '$userid' AND VM_NAME = '$name';";

    $result = $conn->query($sql);

    if ($result->num_rows > 0)
    	{

            while($Row = $result->fetch_assoc())
            {
                $rows= $Row;
            }

            return $response->withJson($rows);

        }
    	else
    	{
            return $response->withJson($result,404);
        }

});

$app->get('/vmstart/{userid}/{name}[/]', function ($request, $response, $args) {

    $route = $request->getAttribute('route');

    $userid = $route->getArgument('userid');
    $name = $route->getArgument('name');

    // DBCONFIG
    require '../src/Connections.php';

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "INSERT INTO HISTORY ( ACTIVITY , UID ) VALUES ('Started " . $name . "' , " . $userid . ")";
    $result = $conn->query($sql);
    
    $conn->close();
    exit();
});

$app->get('/vmstop/{userid}/{name}[/]', function ($request, $response, $args) {

    $route = $request->getAttribute('route');

    $userid = $route->getArgument('userid');
    $name = $route->getArgument('name');

    // DBCONFIG
    require '../src/Connections.php';

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "INSERT INTO HISTORY ( ACTIVITY , UID ) VALUES ('Stoped " . $name . "' , " . $userid . ")";
    $result = $conn->query($sql);
    
    $conn->close();
    exit();
});
$app->get('/vmremove/{userid}/{name}[/]', function ($request, $response, $args) {

    $route = $request->getAttribute('route');

    $userid = $route->getArgument('userid');
    $name = $route->getArgument('name');

    // DBCONFIG
    require '../src/Connections.php';

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    
   
    
    $sql = "DELETE FROM VMDETAILSPOOL WHERE VID = ( SELECT VID FROM VMCONFIGPOOL WHERE UID = " .$userid ." AND VM_NAME = '" . $name . "' )";
    $result = $conn->query($sql);
    
    
    $sql = "DELETE FROM VMCONFIGPOOL WHERE UID = " .$userid ." AND VM_NAME = '" . $name . "'";
    $result = $conn->query($sql);
    

    $sql = "INSERT INTO HISTORY ( ACTIVITY , UID ) VALUES ('Remove " . $name . "' , " . $userid . ")";
    $result = $conn->query($sql);
    
    $conn->close();
    exit();
});

?>
