<?php

/*
 * Copyright (C) 2018 VanGiex
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use Slim\Http\Request;
use Slim\Http\Response;

// Routes Config
require "Routes/vmroute.php";



// Navigation
$app->get('[/]', function ($request, $response, $args) {
    require '../src/Models/login/body.php';
    exit();
});

$app->get('/index[/]', function ($request, $response, $args) {
    require '../src/Models/login/body.php';
    exit();
});

$app->POST('/signin[/]', function ($request, $response, $args) {
    $user = $_POST["username"];
    $pass = $_POST["password"];

    if ($user != "" && $pass != "") {

        // DBCONFIG
        require 'Connections.php';

        // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);

        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        $sql = "SELECT UID , PASSWORD FROM LOGIN WHERE USERNAME = '$user'";

        $result = $conn->query($sql);

        if ($result->num_rows > 0) {

            while ($Row = $result->fetch_assoc()) {
                if ($Row['PASSWORD'] == $pass) {
                    session_start();
                    $_SESSION['Session_Start'] = "True";
                    $_SESSION["USER"] = $user;
                    $_SESSION["UID"] = $Row['UID'];
                    $user = strtoupper($user);
                    $sql = "INSERT INTO HISTORY ( ACTIVITY , UID ) VALUES ('LOGIN WITH $user' , " . $Row['UID'] . ")";
                    $result = $conn->query($sql);
                    header("location: dashboard");
                    $conn->close();
                    exit();
                }
            }
        }

        header("location: index.php?state=fail");
        $conn->close();
    } elseif (session_start() && $_SESSION['Session_Start'] == "True") {
        header("location: dashboard");
        exit();
    } else {
        header("location: index.php");
        exit();
    }
    exit();
});



$app->get('/dashboard[/]', function ($request, $response, $args) {
    require 'Models/header/header.php';
    require 'Models/dashboard/body.php';
    require 'Models/footer/footer.php';
    exit();
});


$app->get('/labs[/]', function ($request, $response, $args) {
    require 'Models/header/header.php';
    require 'Models/labs/body.php';
    require 'Models/footer/footer.php';
    exit();
});

$app->get('/newlab[/]', function ($request, $response, $args) {
    require 'Models/header/header.php';
    require 'Models/newlab/body.php';
    require 'Models/footer/footer.php';
    exit();
});

$app->get('/profile[/]', function ($request, $response, $args) {
    require 'Models/header/header.php';
    require 'Models/profile/body.php';
    require 'Models/footer/footer.php';
    exit();
});

$app->get('/register[/]', function ($request, $response, $args) {

    require 'Models/register/body.php';
    exit();
});


$app->POST('/register[/]', function ($request, $response, $args) {

    require 'Models/register/body.php';


    $FNAME = $_POST['FNAME'];
    $PASSWORD = $_POST['PASSWORD'];

    IF ($FNAME != '' && $PASSWORD != '') {
        $CPASSWORD = $_POST['CPASSWORD'];
        $FB = $_POST['FB'];
        $TW = $_POST['TW'];
        $LNAME = $_POST['LNAME'];
        $EMAIL = $_POST['EMAIL'];
        $PHONE = $_POST['PHONE'];
        $BIO = $_POST['BIO'];

        require 'Connections.php';

        // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);

        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        $sql1 = "INSERT INTO `USERS`(`FNAME`, `LNAME`, `EMAIL`, `PHONE`, `BIO`, `FACEBOOK`, `TWITER`) VALUES ('$FNAME','$LNAME','$EMAIL','$PHONE','$BIO','$FB','$TW')";
        $sql2 = "INSERT INTO `LOGIN`(`UID`, `USERNAME`, `PASSWORD`) VALUES ((SELECT UID FROM USERS WHERE EMAIL = '$EMAIL' AND PHONE = '$PHONE'),'$EMAIL','$PASSWORD')";

        $user = strtoupper($EMAIL);
        $sql3 = "INSERT INTO HISTORY ( ACTIVITY , UID ) VALUES ('REGISTERED WITH $user' , (SELECT UID FROM USERS WHERE EMAIL = '$EMAIL' AND PHONE = '$PHONE'))";

        if ($conn->query($sql1) === TRUE && $conn->query($sql2) === TRUE && $conn->query($sql3) === TRUE) {
            echo '<script type="text/javascript">'
            . 'alert("Registration Success!!");'
            . '</script>';
        } else {
            echo '<script type="text/javascript">'
            . 'alert("Registration Failed Error 0x1 Check FAQ For More Info");'
            . '</script>';
        }
        $conn->close();
    }
    exit();
});

$app->get('/todolist[/]', function ($request, $response, $args) {
    require 'Models/header/header.php';
    require 'Models/todolist/body.php';
    require 'Models/footer/footer.php';
    exit();
});

$app->POST('/todolist[/]', function ($request, $response, $args) {

    session_start();
    require 'Connections.php';
    
    $title = $_POST['title'];
    $note = $_POST['note'];
    
    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "INSERT INTO TODOLIST ( TITLE , NOTE , UID ) VALUES (' $title' ,' $note'," . $_SESSION['UID'] . ")";
    $result = $conn->query($sql);
    $conn->close();
    
    header("location: todolist");
    exit();
});

$app->get('/logout[/]', function ($request, $response, $args) {
    session_start();
    require 'Connections.php';

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "INSERT INTO HISTORY ( ACTIVITY , UID ) VALUES ('LOGOUT WITH " . strtoupper($_SESSION["USER"]) . "' , " . $_SESSION['UID'] . ")";
    $result = $conn->query($sql);
    $conn->close();

    //require'Models/logout/body.php';
    unset($_SESSION["Session_Start"]);
    session_destroy();
    $_SESSION = array();
    header("location: index.php");
    exit();
});

$app->get('/learners[/]', function ($request, $response, $args) {
    session_start();
    require 'Connections.php';

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "INSERT INTO HISTORY ( ACTIVITY , UID ) VALUES ('LEARNERS BOARD WITH " . strtoupper($_SESSION["USER"]) . "' , " . $_SESSION['UID'] . ")";
    $result = $conn->query($sql);
    $conn->close();

    header("location: https://onlinecourses.nptel.ac.in/");
    exit();
});



// Debug info
$app->get('/server[/]', function ($request, $response, $args) {
    PHPinfo();
    exit();
});
