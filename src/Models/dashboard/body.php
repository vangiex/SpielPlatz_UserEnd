<?php ?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">My Dashboard</li>
        </ol>
        <!-- Icon Cards-->
        <div class="row">
            <div class="col-xl-3 col-sm-6 mb-3">
                <div class="card text-white bg-primary o-hidden h-100">
                    <div class="card-body">
                        <div class="card-body-icon">
                            <i class="fa fa-fw fa-comments"></i>
                        </div>
                        <div class="mr-5">New Messages!</div>
                    </div>
                    <a class="card-footer text-white clearfix small z-1" href="bhavika">
                        <span class="float-left">Write</span>
                        <span class="float-right">
                            <i class="fa fa-angle-right"></i>
                        </span>
                    </a>
                </div>
            </div>
            <div class="col-xl-3 col-sm-6 mb-3">
                <div class="card text-white bg-warning o-hidden h-100">
                    <div class="card-body">
                        <div class="card-body-icon">
                            <i class="fa fa-fw fa-list"></i>
                        </div>
                        <div class="mr-5">To Do List!</div>
                    </div>
                    <a class="card-footer text-white clearfix small z-1" href="todolist">
                        <span class="float-left">View Details</span>
                        <span class="float-right">
                            <i class="fa fa-angle-right"></i>
                        </span>
                    </a>
                </div>
            </div>
            <div class="col-xl-3 col-sm-6 mb-3">
                <div class="card text-white bg-success o-hidden h-100">
                    <div class="card-body">
                        <div class="card-body-icon">
                            <i class="fa fa-fw fa-shopping-cart"></i>
                        </div>
                        <div class="mr-5">New Lab</div>
                    </div>
                    <a class="card-footer text-white clearfix small z-1" href="newlab">
                        <span class="float-left">Create</span>
                        <span class="float-right">
                            <i class="fa fa-angle-right"></i>
                        </span>
                    </a>
                </div>
            </div>
            <div class="col-xl-3 col-sm-6 mb-3">
                <div class="card text-white bg-danger o-hidden h-100">
                    <div class="card-body">
                        <div class="card-body-icon">
                            <i class="fa fa-fw fa-support"></i>
                        </div>
                        <div class="mr-5">Community Board</div>
                    </div>
                    <a class="card-footer text-white clearfix small z-1" href="divya">
                        <span class="float-left">Ask Us!</span>
                        <span class="float-right">
                            <i class="fa fa-angle-right"></i>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <!-- Area Chart Example-->
        <div class="card mb-3">
            <div class="card-header">
                <i class="fa fa-area-chart"></i>Usage History</div>
            <div class="card-body">

                <table class="table table-hover table-responsive-sm table-responsive-md mb-0 pb-0 ">
                    <tbody>
                        
                        <?php
                        // DBCONFIG
                        require '../src/Connections.php';

                        // Create connection
                        $conn = new mysqli($servername, $username, $password, $dbname);

                        // Check connection
                        if ($conn->connect_error) {
                            die("Connection failed: " . $conn->connect_error);
                        }

                        $sql = "select ACTIVITY , date_format(ATIME, '%Y-%m-%d %h:%i')AS ATIME from HISTORY where UID = " . $_SESSION['UID'] . " ORDER BY ATIME DESC LIMIT 6";

                        $result = $conn->query($sql);

                        if ($result->num_rows > 0) {
                            while ($Row = mysqli_fetch_row($result)) {
                                echo "<tr>";
                                echo '<td>' . $Row[1] . '</td>' .
                                     '<td>' . $Row[0] . 
                                     '</td> </tr>';
                            }
                        }
                        ?>
                        
                    </tbody>
                </table>

            </div>
            <div class="card-footer small text-muted">Updated Today</div>
        </div>

        <div class="card">
            <!-- Card Columns Example Social Feed-->
            <div class="card-header">
                <span class="fa fa-newspaper-o">&nbsp;News Feed</span>
            </div>

            <div class="card-columns">
                <?php
                $url = 'https://newsapi.org/v2/everything?q=computer%20technology&pageSize=75&sortBy=popularity&language=en&apiKey=1d9f0aa9292b41c5b0444c616e487794';
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_TIMEOUT, 5);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $data = curl_exec($ch);
                curl_close($ch);
                $data = json_decode($data);
                $data = $data->articles;

                $limit = 25;
                for ($i = 0; $i < sizeof($data) - 1 && $i < $limit; $i++) {

                    if ($data[$i]->urlToImage == null) {
                        $limit++;
                        continue;
                    }
                    echo'
                                <div class="card mb-2">
                                    <a>
                                            <img class="card-img-top img-fluid w-100" src="' . $data[$i]->urlToImage . '" alt="Tech Feed">
                                    </a>

                                    <div class="card-body">
                                                <h6 class="card-title mb-1"><a>' . $data[$i]->title . '</a></h6>
                                                            <p class="card-text small">' . $data[$i]->description . '<a href="' . $data[$i]->url . '">#' . $data[$i]->source->name . '</a></p>
                                    </div>

                                    <div class="card-footer small text-muted">Published&nbsp;At: ' . $data[$i]->publishedAt .
                    '</div>
                                </div>';
                }
                ?>
            </div>
            <!-- /Card Columns-->
        </div>
    </div>


