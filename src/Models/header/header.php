<?php

/*
 * Copyright (C) 2018 VanGiex
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

if (session_start() && $_SESSION['Session_Start'] != "True") {
     header("location: index.php?state=fail");
}

 ?>
        <!DOCTYPE html>
        <html lang="en">

        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <meta name="description" content="">
            <meta name="author" content="">
            <title>SpielPlatZ | Window</title>
            <!-- Bootstrap core CSS-->
            <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
            <!-- Custom fonts for this template-->
            <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
            <!-- Page level plugin CSS-->
            <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
            <!-- Custom styles for this template-->
            <link href="../css/sb-admin.css" rel="stylesheet">
        </head>

        <body class="fixed-nav sticky-footer bg-dark" id="page-top">
            <!-- Navigation-->
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
                <a class="navbar-brand" href="#">SpielPlatZ</a>
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">

                        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
                            <a class="nav-link" href="dashboard">
                                <i class="fa fa-fw fa-dashboard"></i>
                                <span class="nav-link-text">Dashboard</span>
                            </a>
                        </li>

                        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
                            <a class="nav-link" href="profile">
                                <i class="fa fa-fw fa-users"></i>
                                <span class="nav-link-text">My Profile</span>
                            </a>
                        </li>

                        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
                            <a class="nav-link" href="todolist">
                                <i class="fa fa fa-sticky-note"></i>
                                <span class="nav-link-text">My Notes</span>
                            </a>
                        </li>

                        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
                            <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents" data-parent="#exampleAccordion">
                                <i class="fa fa-fw fa-cubes"></i>
                                <span class="nav-link-text">PlayGround</span>
                            </a>
                            <ul class="sidenav-second-level collapse" id="collapseComponents">
                                <li>
                                    <a href="newlab">New Lab</a>
                                </li>
                                <li>
                                    <a href="labs">Start Lab</a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Tables">
                            <a class="nav-link" href="bhavika">
                                <i class="fa fa-fw fa-telegram"></i>
                                <span class="nav-link-text">Message Board</span>
                            </a>
                        </li>

                        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Charts">
                            <a class="nav-link" href="divya">
                                <i class="fa fa-life-ring"></i>
                                <span class="nav-link-text">Comunity Board</span>
                            </a>
                        </li>

                        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Charts">
                                <a class="nav-link" href="learners">
                                <i class="fa fa-laptop"></i>
                                <span class="nav-link-text">Learners Board</span>
                            </a>
                        </li>

                    </ul>
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="fa text-white" href="logout"><i class="fa fa-fw fa-sign-out"></i>Logout</a>
                        </li>
                    </ul>

                </div>
            </nav>
