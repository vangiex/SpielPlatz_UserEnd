<?php
/*
 * Copyright (C) 2018 VanGiex
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="dashboard">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">My Notes Area</li>
        </ol>

        <div class="row mb-1">

            <div class="col-md-12 card-body">
                <form  method="post">

                    <label for="title">Title</label>
                    <input class="form-control" name="title" id="title" max="30" type="text" aria-describedby="nameHelp" placeholder="Note Title">
                    <br>

                    <label for="note">Note</label>
                    <textarea class="form-control" name="note" maxlength="960" aria-describedby="nameHelp" placeholder="My Notes"></textarea>
                    <br>

                    <button  class="btn btn-primary btn-block" type="submit" >NOTE IT</button>
                    <br>
                </form>
            </div>
        </div>
        
        <?php
        // DBCONFIG
        require '../src/Connections.php';

        // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);

        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        $sql = "select TITLE , NOTE , date_format(ATIME, '%Y-%m-%d %h:%i')AS ATIME from TODOLIST where UID = " . $_SESSION['UID'] . " ORDER BY ATIME DESC LIMIT 6";

        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            while ($Row = mysqli_fetch_row($result)) {
                
                echo '<div class="row mb-1">';

                echo '<div class="col-md-12">';

                echo '<div class="card mb-2">';

                echo '<div class="card-header pb-0 mb-0 pt-0">';

                echo '<h4>'.$Row[0].'</h4>';

                echo '</div>';

                echo '<div class="card-body">';

                echo '<p>'.$Row[1].'</p>';

                echo '</div>';

                echo '<div class="card-footer small text-muted">Published : '.$Row[2].' </div>';

                echo '</div>';
                
                echo '</div>';
                
                echo '</div>';
                
            }
        }
        $conn->close();
        ?>
        
    </div>
</div>
