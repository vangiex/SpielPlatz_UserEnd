<?php
// DBCONFIG
require '../src/Connections.php';

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "select * from USERS WHERE UID = " . $_SESSION['UID'];

$result = $conn->query($sql);

if ($result->num_rows > 0) {
    $Row = mysqli_fetch_row($result);
}

$conn->close();

?>

<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="dashboard">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Profile</li>
        </ol>
        <div>
            <div class="card mx-auto mb-3">
                <div class="card-body">
                    <form>
                        <div class="form-group">
                            <div class="form-row">
                                <div class="col-md-4">
                                    <label for="exampleInputName">First name</label>
                                    <input class="form-control" id="exampleInputName" name="FNAME" type="text" aria-describedby="nameHelp" value= <?php echo $Row[1]; ?> disabled="" >
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="exampleInputLastName">Last name</label>
                                    <input class="form-control" id="exampleInputLastName" name="LNAME"type="text" aria-describedby="nameHelp" value= <?php echo $Row[2]; ?> disabled="" >
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input class="form-control" id="exampleInputEmail1" name="EMAIL" type="email" aria-describedby="emailHelp" value= <?php echo $Row[3]; ?> disabled="" >
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Phone Number</label>
                            <input class="form-control" id="exampleInputEmail1" name="EMAIL" type="email" aria-describedby="emailHelp" value= <?php echo $Row[4]; ?> disabled="" >
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPhone">About You</label>
                            <input class="form-control" id="exampleInputPhone" type="text" name="PHONE" aria-describedby="phoneHelp" value= <?php echo $Row[5]; ?> disabled="" >
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon">    <i class="fa fa-facebook" aria-hidden="true"></i>
                            </span>
                            <input class="form-control" id="exampleInputFacebookId" name="SOCIAL_LINK" type="text" aria-describedby="facebookidHelp" value= <?php if($Row[6]) { echo $Row[6]; } else { echo 'N/A'; } ?> disabled="" >
                        </div>
                        <br/>
                        <div class="input-group">
                            <span class="input-group-addon">    <i class="fa fa-twitter" aria-hidden="true"></i>
                            </span>
                            <input class="form-control" id="exampleInputTwitterId" name="SOCIAL_LINK" type="text" aria-describedby="twitteridHelp" value= <?php if($Row[7]) { echo $Row[6]; } else { echo 'N/A'; } ?> disabled="" >
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
