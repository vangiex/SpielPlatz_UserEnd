
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="dashboard">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">My Lab</li>
        </ol>


        <div class="row mb-3 container">

            <div class="col-md-12 pr-0 pl-0">
                <div class="alert alert-info">
                    <strong>Note!</strong> Lab  may take 30-60 sec to start. Lab will not shutdown on close.
                </div>
            </div>
            <?php
            /* @var $_GET type */
           
            if(isset($_GET['warn']) && $_GET['warn'] == 'true') {
                echo '<div class="col-md-12 pr-0 pl-0">';
                echo '<div class="alert alert-warning">';
                echo '<strong>Note!</strong> Lab may take 60-180 sec to deploy. Username and Password will display on Deploy.';
                echo '</div>';
                echo '</div>';
            }
            ?>

            <table class="table table-responsive-lg table-responsive-md table-responsive-sm">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">User</th>
                        <th scope="col">Pass</th>
                        <th scope="col">Os</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    // DBCONFIG
                    require '../src/Connections.php';

                    // Create connection
                    $conn = new mysqli($servername, $username, $password, $dbname);

                    // Check connection
                    if ($conn->connect_error) {
                        die("Connection failed: " . $conn->connect_error);
                    }

                    $sql = "SELECT A.VID , VM_NAME , USERNAME AS 'USER' , PASSWORD AS 'PASS' , OS , NETWORK_SUPPORT FROM `VMCONFIGPOOL` AS A JOIN VMDETAILSPOOL AS B WHERE A.VID = B.VID AND UID = " . $_SESSION['UID'];

                    $result = $conn->query($sql);

                    if ($result->num_rows > 0) {
                        while ($Row = mysqli_fetch_row($result)) {
                            $a = sprintf(" '%s' ", $_SESSION['UID']);
                            $b = sprintf(" '%s' ", $Row[1]);
                            echo "<tr>";
                            echo '<th scope = "row">' . $Row[0] . '</th>' .
                            '<td>' . $Row[1] . '</td>' .
                            '<td>' . $Row[2] . '</td>' .
                            '<td>' . $Row[3] . '</td>' .
                            '<td>' . $Row[4] . '</td>' .
                            '<td>' . $Row[5] . '</td>' .
                            '<td>' .
                            '<input type="button" class="btn btn-outline-primary pl-2" onclick = "action(' . $a . ' ,' . $b . ', 1 );" value="Launch"/>'
                            . '&nbsp;' .
                            '<input type="button" class="btn btn-outline-warning pl-2" onclick="action(' . $a . ',' . $b . ', 2 );" value="Stop"/>' .
                            '&nbsp;' .
                            '<input type="button" class="btn btn-outline-danger pl-2" onclick="action(' . $a . ',' . $b . ', 3 );" value="Delete"/>'
                            . '</td>';
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
<script type="text/javascript">
    function sleep(delay) {
        var start = new Date().getTime();
        while (new Date().getTime() < start + delay)
            ;
    }

    function action(userid, name, act)
    {
        var r = <?php
                    require '../src/applicationproperties.php';
                    echo "'" . $tunneluri . "'";
                    ?> + "?uid=" + userid + "&name=" + name;

        if (act === 1)
        {
            $.get(<?php
                    require '../src/applicationproperties.php';
                    echo "'" . $serviceuri . "'";
                    ?>, {"userid": userid, "name": name, "action": act});
            sleep(2000);
            window.open(r, '', 'left=20,top=20,width=600,height=460,directories=0,titlebar=0,toolbar=0,location=0,status=0,menubar=0,scrollbars=no,resizable=no');
        } else if (act === 3)
        {
            $.get(<?php
                    require '../src/applicationproperties.php';
                    echo "'" . $serviceuri . "'";
                    ?>, {"userid": userid, "name": name, "action": act});
        } else if (act === 2)
        {
            sleep(1000);
            $.get(<?php
                    require '../src/applicationproperties.php';
                    echo "'" . $serviceuri . "'";
                    ?>, {"userid": userid, "name": name, "action": act});
            location.reload();
        }

    }
</script>
