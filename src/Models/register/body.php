<?php

 ?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>SpielPlatz | Register</title>
        <!-- Bootstrap core CSS-->
        <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom fonts for this template-->
        <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- Custom styles for this template-->
        <link href="../css/sb-admin.css" rel="stylesheet">
        <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    </head>

    <body class="bg-dark">
        <div class="container">
            <div class="card card-register mx-auto mt-5">
                <div class="card-header">Register an Account</div>
                <div class="card-body">
                    <form method="post">
                        <div class="form-group">
                            <div class="form-row">
                                <div class="col-md-6">
                                    <label for="exampleInputName">First name</label>
                                    <input class="form-control" id="exampleInputName" name="FNAME" type="text" aria-describedby="nameHelp" placeholder="Enter first name" value="">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="exampleInputLastName">Last name</label>
                                    <input class="form-control" id="exampleInputLastName" name="LNAME"type="text" aria-describedby="nameHelp" placeholder="Enter last name" value="">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input class="form-control" id="exampleInputEmail1" name="EMAIL" type="email" aria-describedby="emailHelp" placeholder="Enter email" value="">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPhone">Phone Number</label>
                            <input class="form-control" id="exampleInputPhone" type="text" name="PHONE" aria-describedby="phoneHelp" placeholder="Phone Number" value="">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPhone">About You</label>
                            <input class="form-control" id="exampleInputPhone" type="text" name="BIO" aria-describedby="phoneHelp" placeholder="About" value="">
                        </div>
                        <div class="form-group">
                            <div class="form-row">
                                <div class="col-md-6">
                                    <label for="exampleInputPassword1">Password</label>
                                    <input class="form-control" id="exampleInputPassword1" type="password" name="PASSWORD" placeholder="Password" value="">
                                </div>
                                <div class="col-md-6">
                                    <label for="exampleConfirmPassword">Confirm password</label>
                                    <input class="form-control" id="exampleConfirmPassword" type="password" name="CPASSWORD" placeholder="Confirm password" value="">
                                </div>
                            </div>
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon">    <i class="fa fa-facebook" aria-hidden="true"></i>
                            </span>
                            <input class="form-control" id="exampleInputFacebookId" name="FB" type="text" aria-describedby="facebookidHelp" placeholder="Facebook Profile Link" value="">
                        </div>
                        <br/>
                        <div class="input-group">
                            <span class="input-group-addon">    <i class="fa fa-twitter" aria-hidden="true"></i>
                            </span>
                            <input class="form-control" id="exampleInputTwitterId" name="TW" type="text" aria-describedby="twitteridHelp" placeholder="Twitter Profile Link" value="">
                        </div>
                        <br/>

                        <button class="btn btn-primary btn-block" type="submit" >Register</button>
                    </form>
                    <div class="text-center">
                        <a class="d-block small mt-3" href="index.php">Login Page</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Bootstrap core JavaScript-->
        <script src="../vendor/jquery/jquery.min.js"></script>
        <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    </body>
</html>
